## Task Steps
1. You were provided with materials to read so that you can learn about     Distributed Version Control Systems.    
2. After learning about this you are to fork/clone a public repository that is available on bitbucket.org on this link https://bitbucket.org/hrdm1393/dmhalloworld.git
3. After forking/cloning this project you are to modify the MySelf class so that it tells us more about yourself. After the modification you will send us a link to your forked public repository so that we can see your modifications. The project was created using Eclipse IDE. Any IDE or text editor can be used to complete the class modification.    
4. If you think you have mastered pull requests you can send a pull request to the original repository. ** This would show that one has mastered the basics of version control, but ending at step 3 is good enough.
